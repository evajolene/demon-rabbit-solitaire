Card meets matching game, featuring a demon rabbit. Check it out here: https://ldjam.com/events/ludum-dare/43/demon-rabbit-solitaire

License: You can't directly copy any of my code or assets for use in commercial projects without my permission, except the Domine font which is not mine: see Assets/OFL.txt for the license on that.