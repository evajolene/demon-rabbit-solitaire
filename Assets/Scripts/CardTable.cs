﻿using UnityEngine;

public class CardTable : MonoBehaviour
{
    public CardDefinition[] cards;
}

[System.Serializable]
public struct CardDefinition
{
    public Sprite sprite;
    public ushort value;
    public byte type;
}