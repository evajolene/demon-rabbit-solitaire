﻿using UnityEngine;

public class LifeHand : MonoBehaviour
{
    public Card[] cards;
    private SpriteRenderer[] renderers;
    public const int Count = 3;

    public Sprite lifeCardSprite;

    void Start()
    {
        cards = new Card[Count];
        renderers = new SpriteRenderer[Count];
        for (int index = 0; index < Count; index++)
        {
            byte cardIndex = (byte)Random.Range(0, 7);
            cards[index].index = cardIndex;
            GameObject cardObject = new GameObject("Card");
            renderers[index] = cardObject.AddComponent<SpriteRenderer>();
            renderers[index].sprite = lifeCardSprite;
            cardObject.transform.position = new Vector3(7.5f + index * 1.25f, -2, 0);
        }
    }

    void Update()
    {
        Vector3 newPosition = Vector3.zero;
        for (int index = 0; index < Count; index++)
        {
            renderers[index].enabled = !cards[index].isSacrificed;
            if (cards[index].isSacrificed)
            {
                continue;
            }
            newPosition.x = 7.5f + index * 1.25f;
            newPosition.y = -2 + ((cards[index].isHovered || cards[index].isSelected) ? 0.175f : 0.0f);
            cards[index].isHovered = false;
            renderers[index].gameObject.transform.position = newPosition;
        }
    }
}
