﻿using UnityEngine;

public class Table : MonoBehaviour
{
    public Card[,] cards;
    private SpriteRenderer[,] renderers;
    public const int Columns = 6;
    public const int Rows = 2;

    public Sprite cardBackSprite;
    public CardTable cardTable;

    void Start()
    {
        cards = new Card[Columns, Rows];
        renderers = new SpriteRenderer[Columns, Rows];
        for (int x = 0; x < Columns; x++)
        {
            for (int y = 0; y < Rows; y++)
            {
                cards[x, y].index = (byte)Random.Range(0, 7);
                GameObject cardObject = new GameObject("Card");
                renderers[x, y] = cardObject.AddComponent<SpriteRenderer>();
                renderers[x, y].sprite = cardBackSprite;
                cardObject.transform.position = new Vector3(x * 2, y * 2, 0);
            }
        }
    }

    void Update()
    {
        Vector3 newPosition = Vector3.zero;
        for (int x = 0; x < Columns; x++)
        {
            for (int y = 0; y < Rows; y++)
            {
                newPosition.x = x * 2;
                newPosition.y = y * 2 + ((cards[x, y].isHovered || cards[x, y].isSelected) ? 0.175f : 0.0f);
                renderers[x, y].gameObject.transform.position = newPosition;
                if (cards[x, y].isSacrificed)
                {
                    renderers[x, y].sprite = cardTable.cards[cards[x, y].index].sprite;
                    renderers[x, y].enabled = cards[x, y].isHovered;
                }
                else
                {
                    renderers[x, y].enabled = true;
                    renderers[x, y].sprite = 
                        cards[x, y].isSelected ? cardTable.cards[cards[x, y].index].sprite : cardBackSprite;
                }
                cards[x, y].isHovered = false;
            }
        }
    }
}

public struct Card
{
    public byte index;
    public bool isHovered;
    public bool isSelected;
    public bool isSacrificed;
}