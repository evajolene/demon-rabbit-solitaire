﻿using UnityEngine;

public class TiledBackground : MonoBehaviour
{
    private Mesh mesh;
    private const int Columns = 12;
    private const int Rows = 5;
    private GameObject meshObject;

    private new Camera camera;

    public Texture image;
    public Material material;

    void Start()
    {
        mesh = new Mesh();

        float aspectRatio = (float)Columns / Rows;
        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(0, 1, 0);
        vertices[2] = new Vector3(aspectRatio, 1, 0);
        vertices[3] = new Vector3(aspectRatio, 0, 0);

        Vector2[] uvs = new Vector2[]
        {
            new Vector2(0, 0),
            new Vector2(0, Rows),
            new Vector2(Columns, Rows),
            new Vector2(Columns, 0)
        };

        int[] indices = new int[]
        {
            0, 1, 2, 0, 2, 3
        };

        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = indices;

        meshObject = new GameObject("Background Mesh");
        MeshFilter meshFilter = meshObject.AddComponent<MeshFilter>();
        meshFilter.sharedMesh = mesh;
        MeshRenderer meshRenderer = meshObject.AddComponent<MeshRenderer>();
        meshRenderer.sharedMaterial = material;

        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 cameraPosition = camera.transform.position;
        float aspectRatio = (float)Columns / Rows;

        float halfScale = camera.orthographicSize;
        if (aspectRatio > 1.0f)
        {
            halfScale *= aspectRatio;
        }
        meshObject.transform.localScale = new Vector3(halfScale * 2, halfScale * 2, 1);
        Vector3 centeredPosition = new Vector3(cameraPosition.x - aspectRatio * halfScale, cameraPosition.y - halfScale, 1);
        Vector3 mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mouseOffsetFromCenter = mousePosition - cameraPosition;
        mouseOffsetFromCenter.z = 0.0f;
        float magnitude = mouseOffsetFromCenter.magnitude;
        magnitude = Mathf.Min(1.0f, magnitude / (camera.orthographicSize * aspectRatio));
        mouseOffsetFromCenter.Normalize();
        meshObject.transform.position = centeredPosition - mouseOffsetFromCenter * magnitude * 0.75f;
    }
}
