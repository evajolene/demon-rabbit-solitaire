﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour
{
    private new Camera camera;
    private string[] monologue;
    private string[] goodJobStrings;

    public Text text;

    void Start()
    {
        camera = Camera.main;
        monologue = new string[]
        {
            "Hi there! I'm Demon, the demon rabbit!",
            "I'm starving, so hurry and match these cards!",
            "Else I'll have to eat you!"
        };
        goodJobStrings = new string[]
        {
            "Yummy!",
            "Not bad!",
            "Delicious!",
            "Tasty!",
            "*munch munch*"
        };
        StartCoroutine(SayStuff());
    }

    void Update()
    {
        text.rectTransform.pivot = new Vector2(0, 0.5f);
        Vector3 screenPoint = camera.WorldToScreenPoint(new Vector3(1, 4.0f, 0));
        screenPoint.x = Mathf.RoundToInt(screenPoint.x);
        screenPoint.y = Mathf.RoundToInt(screenPoint.y);
        text.rectTransform.anchoredPosition = screenPoint;
    }

    private IEnumerator SayStuff()
    {
        for (int index = 0; index < monologue.Length; index++)
        {
            yield return new WaitForSeconds(0.75f);
            string[] words = monologue[index].Split(' ');
            string line = "";
            for (int word = 0; word < words.Length; word++)
            {
                for (int character = 0; character < words[word].Length; character++)
                {
                    line += words[word][character];
                    text.text = line;
                    yield return new WaitForSeconds(0.01f);
                }
                line += " ";
                if (words[word].Contains(",") || words[word].Contains("!"))
                {
                    yield return new WaitForSeconds(0.7f);
                }
                else
                {
                    yield return new WaitForSeconds(0.04f);
                }
            }
        }

        int score = GameController.score;
        while (true)
        {
            yield return new WaitForSeconds(0.1f);

            string[] words;
            bool isGameOver = false;
            if (GameController.score > score)
            {
                int index = Random.Range(0, goodJobStrings.Length);
                words = goodJobStrings[index].Split(' ');
            }
            else if (GameController.state == GameController.State.GameOver)
            {
                string gameOverText = "Hahaha! You're dead! Score: " + GameController.score.ToString();
                words = gameOverText.Split(' ');
                isGameOver = true;
            }
            else
            {
                continue;
            }
            string line = "";
            for (int word = 0; word < words.Length; word++)
            {
                for (int character = 0; character < words[word].Length; character++)
                {
                    line += words[word][character];
                    text.text = line;
                    yield return new WaitForSeconds(0.01f);
                }
                line += " ";
                if (words[word].Contains(",") || words[word].Contains("!"))
                {
                    yield return new WaitForSeconds(0.7f);
                }
                else
                {
                    yield return new WaitForSeconds(0.04f);
                }
            }
            score = GameController.score;
            if (isGameOver)
            {
                break;
            }
        }
    }
}
