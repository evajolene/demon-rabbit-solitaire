﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemonUIController : MonoBehaviour
{
    private new Camera camera;

    public Image image;
    public Image fillImage;

    void Start()
    {
        camera = Camera.main;
    }

    void Update()
    {
        image.rectTransform.pivot = new Vector2(1, 0.5f);
        fillImage.rectTransform.pivot = new Vector2(1, 0);
        Vector3 screenPoint = camera.WorldToScreenPoint(new Vector3(10.5f, 4, 0));
        screenPoint.x = Mathf.RoundToInt(screenPoint.x);
        screenPoint.y = Mathf.RoundToInt(screenPoint.y);
        image.rectTransform.anchoredPosition = screenPoint;
        screenPoint.y -= 30;
        fillImage.rectTransform.anchoredPosition = screenPoint;
        int height = Mathf.FloorToInt((float)GameController.score / GameController.targetScore * 60) % 60;
        fillImage.rectTransform.sizeDelta = new Vector2(60, height);
    }
}
