﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public static State state;
    private int cardInHandSelected;
    private XY[] cardsOnTableSelected;
    private bool isTableCleared;

    private Coroutine coroutine;

    public Table table;
    public Hand hand;
    public LifeHand lifeHand;
    public CardTable cardTable;
    public AudioSource flipCardAudioSource;
    public AudioSource scoreAudioSource;
    public AudioSource hurtAudioSource;

    public static int score = 0;
    public const int targetScore = 60;

    void Start()
    {
        state = State.SelectFromTable;

        cardInHandSelected = -1;
        cardsOnTableSelected = new XY[2];
        cardsOnTableSelected[0] = new XY()
        {
            x = -1, y = -1
        };
        cardsOnTableSelected[1] = new XY()
        {
            x = -1,
            y = -1
        };
    }

    void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        if (state == State.SelectFromHand || state == State.SelectFromTable)
        {
            int index = Mathf.FloorToInt(mousePosition.x / 1.25f - 5.5f);
            if
            (
                index >= 0 && index < LifeHand.Count && mousePosition.y < -1.0f &&
                !lifeHand.cards[index].isSacrificed
            )
            {
                lifeHand.cards[index].isHovered = true;
                if (Input.GetMouseButtonDown(0))
                {
                    lifeHand.cards[index].isSacrificed = true;
                    bool isLifeHandCleared = true;
                    for (index = 0; index < LifeHand.Count; index++)
                    {
                        if (!lifeHand.cards[index].isSacrificed)
                        {
                            isLifeHandCleared = false;
                            break;
                        }
                    }
                    hurtAudioSource.Play();
                    if (isLifeHandCleared)
                    {
                        state = State.GameOver;
                    }
                    else
                    {
                        state = State.MakeNewTable;
                    }
                }
            }
        }

        if (state == State.SelectFromHand)
        {
            int index = Mathf.FloorToInt(mousePosition.x / 1.25f + 0.5f);
            if (index >= 0 && index < Hand.Count && mousePosition.y < -1.0f)
            {
                hand.cards[index].isHovered = true;
                if (Input.GetMouseButtonDown(0))
                {
                    hand.cards[index].isSelected = true;
                    cardInHandSelected = index;
                    state = State.SelectFromTable;
                }
            }
        }
        else if (state == State.SelectFromTable)
        {
            int x = Mathf.FloorToInt(mousePosition.x * 0.5f + 0.5f);
            int y = Mathf.CeilToInt(mousePosition.y * 0.5f - 0.5f);
            if (x >= 0 && x < Table.Columns && y >= 0 && y < Table.Rows)
            {
                if (!table.cards[x, y].isSacrificed && cardInHandSelected == -1)
                {
                    table.cards[x, y].isHovered = true;
                    if (Input.GetMouseButtonDown(0) && !table.cards[x, y].isSelected)
                    {
                        flipCardAudioSource.Play();
                        table.cards[x, y].isSelected = true;
                        if (cardsOnTableSelected[0].x == -1 || cardsOnTableSelected[0].y == -1)
                        {
                            cardsOnTableSelected[0].x = x;
                            cardsOnTableSelected[0].y = y;
                        }
                        else
                        {
                            cardsOnTableSelected[1].x = x;
                            cardsOnTableSelected[1].y = y;
                            state = State.CheckForMatch;
                        }
                    }
                }
                else if (cardInHandSelected >= 0 && table.cards[x, y].isSacrificed)
                {
                    table.cards[x, y].isHovered = true;
                    table.cards[x, y].index = hand.cards[cardInHandSelected].index;
                    if (Input.GetMouseButtonDown(0))
                    {
                        cardsOnTableSelected[0].x = x;
                        cardsOnTableSelected[0].y = y;
                        state = State.PlaceCardFromHand;
                    }
                }
            }
        }
        else if (state == State.CheckForMatch)
        {
            if (coroutine == null)
            {
                coroutine = StartCoroutine(CheckForMatch());
            }
        }
        else if (state == State.PlaceCardFromHand)
        {
            if (coroutine == null)
            {
                coroutine = StartCoroutine(PlaceCardFromHand());
            }
        }
        else if (state == State.MakeNewTable)
        {
            if (coroutine == null)
            {
                coroutine = StartCoroutine(MakeNewTable());
            }
        }
        else if (state == State.GameOver)
        {
            if (coroutine == null)
            {
                coroutine = StartCoroutine(GameOver());
            }
        }
    }

    private IEnumerator GameOver()
    {
        for (int x = 0; x < Table.Columns; x++)
        {
            for (int y = 0; y < Table.Rows; y++)
            {
                table.cards[x, y].isSelected = false;
                table.cards[x, y].isSacrificed = true;
                yield return new WaitForSeconds(0.05f);
            }
        }
        for (int index = 0; index < Hand.Count; index++)
        {
            hand.cards[index].isSacrificed = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator PlaceCardFromHand()
    {
        table.cards[cardsOnTableSelected[0].x, cardsOnTableSelected[0].y] = hand.cards[cardInHandSelected];
        hand.cards[cardInHandSelected].isSacrificed = true;
        hand.cards[cardInHandSelected].isSelected = false;
        yield return new WaitForSeconds(0.2f);
        table.cards[cardsOnTableSelected[0].x, cardsOnTableSelected[0].y].isSelected = false;
        yield return new WaitForSeconds(0.15f);
        hand.cards[cardInHandSelected].index = (byte)Random.Range(0, 7);
        hand.cards[cardInHandSelected].isSacrificed = false;
        yield return null;
        cardsOnTableSelected[0].x = cardsOnTableSelected[0].y = -1;
        cardInHandSelected = -1;
        state = State.SelectFromTable;
        coroutine = null;
    }

    private IEnumerator MakeNewTable()
    {
        cardsOnTableSelected[0].x = cardsOnTableSelected[0].y = -1;
        cardsOnTableSelected[1].x = cardsOnTableSelected[1].y = -1;
        if (cardInHandSelected >= 0)
        {
            hand.cards[cardInHandSelected].isSelected = false;
        }
        cardInHandSelected = -1;
        for (int x = 0; x < Table.Columns; x++)
        {
            for (int y = 0; y < Table.Rows; y++)
            {
                table.cards[x, y].isSelected = false;
                if (isTableCleared)
                {
                    score++;
                }
                else if (table.cards[x, y].isSacrificed == false)
                {
                    table.cards[x, y].isSacrificed = true;
                    yield return new WaitForSeconds(0.125f);
                }
                table.cards[x, y].index = (byte)Random.Range(0, 7);
                table.cards[x, y].isHovered = true;
                table.cards[x, y].isSacrificed = false;
                yield return new WaitForSeconds(0.075f);
            }
        }
        state = State.SelectFromTable;
        isTableCleared = false;
        coroutine = null;
    }

    private IEnumerator CheckForMatch()
    {
        bool isMatching = true;
        CardDefinition previousCardDefinition = cardTable.cards[table.cards[cardsOnTableSelected[0].x, cardsOnTableSelected[0].y].index];
        uint totalValue = previousCardDefinition.value;
        for (int index = 1; index < cardsOnTableSelected.Length; index++)
        {
            byte cardIndex = table.cards[cardsOnTableSelected[index].x, cardsOnTableSelected[index].y].index;
            CardDefinition cardDefinition = cardTable.cards[cardIndex];
            if (cardDefinition.type != previousCardDefinition.type)
            {
                isMatching = false;
                break;
            }
            else
            {
                totalValue += cardDefinition.value;
                previousCardDefinition = cardDefinition;
            }
        }

        if (isMatching)
        {
            yield return new WaitForSeconds(0.5f);
            for (int index = 0; index < cardsOnTableSelected.Length; index++)
            {
                table.cards[cardsOnTableSelected[index].x, cardsOnTableSelected[index].y].isSacrificed = true;
                score += cardTable.cards[table.cards[cardsOnTableSelected[index].x, cardsOnTableSelected[index].y].index].value;
                cardsOnTableSelected[index].x = cardsOnTableSelected[index].y = -1;
                scoreAudioSource.Play();
                yield return new WaitForSeconds(0.15f);
            }
            //Do a check if the table is cleared!
            bool isCleared = true;
            for (int x = 0; x < Table.Columns; x++)
            {
                for (int y = 0; y < Table.Rows; y++)
                {
                    if (!table.cards[x, y].isSacrificed)
                    {
                        isCleared = false;
                    }
                }
            }
            if (isCleared)
            {
                isTableCleared = true;
                state = State.MakeNewTable;
            }
            else
            {
                state = State.SelectFromHand;
            }
        }
        else
        {
            yield return new WaitForSeconds(0.5f);
            for (int index = 0; index < cardsOnTableSelected.Length; index++)
            {
                table.cards[cardsOnTableSelected[index].x, cardsOnTableSelected[index].y].isSelected = false;
                cardsOnTableSelected[index].x = cardsOnTableSelected[index].y = -1;
                yield return new WaitForSeconds(0.15f);
            }
            state = State.SelectFromTable;
        }

        coroutine = null;
    }

    private struct XY
    {
        public int x, y;
    }

    public enum State
    {
        SelectFromHand = 0,
        SelectFromTable = 1,
        CheckForMatch = 2,
        PlaceCardFromHand = 3,
        MakeNewTable = 4,
        GameOver = 5
    }
}
